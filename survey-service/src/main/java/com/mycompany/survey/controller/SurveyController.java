package com.mycompany.survey.controller;

import java.util.UUID;

import javax.validation.Valid;

import com.mycompany.survey.answer.model.UpdateSurveyRequest;
import com.mycompany.survey.service.SurveyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/surveys")
public class SurveyController {

    @Autowired
    private SurveyService surveyService;

    @GetMapping(path = "questions/{questionId}")
    public ResponseEntity<Object> findQuestionWithAnswers(@PathVariable final UUID questionId) {
        return surveyService.findQuestionWithAnswers(questionId);
    }

    @PutMapping(path = "questions/{questionId}")
    public ResponseEntity<Object> doOneSurvey(@PathVariable final UUID questionId, @Valid @RequestBody final UpdateSurveyRequest request) {
        request.setQuestionId(questionId);
        return surveyService.doOneSurvey(request);
    }

    @GetMapping(path = "questions/{questionId}/statistics")
    public ResponseEntity<Object> findStatisticsInOneQuestion(@PathVariable final UUID questionId) {
        return surveyService.findStatisticsInOneQuestion(questionId);
    }
}
