package com.mycompany.survey.service;

import java.util.UUID;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.mycompany.survey.answer.model.UpdateSurveyRequest;
import org.springframework.http.ResponseEntity;

public interface SurveyService {

    String QUESTION_SERVICE_URL = "http://localhost:8084/api/questions";

    String ANSWER_SERVICE_URL = "http://localhost:8085/api/answers";

    ResponseEntity<Object> findQuestionWithAnswers(@Valid @NotNull UUID questionId);

    ResponseEntity<Object> doOneSurvey(@Valid UpdateSurveyRequest request);

    ResponseEntity<Object> findStatisticsInOneQuestion(@Valid @NotNull UUID questionId);
}
