package com.mycompany.survey.service;

import java.util.List;
import java.util.UUID;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.mycompany.survey.answer.model.Answer;
import com.mycompany.survey.answer.model.UpdateSurveyRequest;
import com.mycompany.survey.model.ErrorResponse;
import com.mycompany.survey.model.StatisticsResponse;
import com.mycompany.survey.question.model.Question;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.ResponseEntity.ok;

@Service
@Validated
public class SurveyServiceImpl implements SurveyService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SurveyServiceImpl.class);

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public ResponseEntity<Object> findQuestionWithAnswers(@Valid @NotNull final UUID questionId) {
        Question question;
        try {
            question = restTemplate.getForObject(QUESTION_SERVICE_URL + "/{id}", Question.class, questionId);
        } catch (HttpClientErrorException e) {
            LOGGER.error("Exception when get question", e);
            return new ResponseEntity<>(e.getResponseBodyAsString(), e.getStatusCode());
        }

        List<Answer> answers;
        try {
            answers = restTemplate.exchange(ANSWER_SERVICE_URL + "?questionId={questionId}", GET, null,
                                          new ParameterizedTypeReference<List<Answer>>() {/*EMPTY*/}, questionId).getBody();
        } catch (HttpClientErrorException e) {
            LOGGER.error("Exception when get answer", e);
            return new ResponseEntity<>(new ErrorResponse(e.getResponseBodyAsString(), e.getStatusCode().value()), e.getStatusCode());
        }

        question.setAnswers(answers);

        return ok(question);
    }

    @Override
    public ResponseEntity<Object> doOneSurvey(@Valid final UpdateSurveyRequest request) {
        try {
            restTemplate.put(ANSWER_SERVICE_URL + "/batch-update-selection", request);
        } catch (HttpClientErrorException e) {
            LOGGER.error("Exception when update answers", e);
            return new ResponseEntity<>(new ErrorResponse(e.getResponseBodyAsString(), e.getStatusCode().value()), e.getStatusCode());
        }
        return ok().build();
    }

    @Override
    public ResponseEntity<Object> findStatisticsInOneQuestion(@Valid @NotNull final UUID questionId) {
        final ResponseEntity<Object> responseEntity = findQuestionWithAnswers(questionId);
        final Object questionWithAnswersResponse = responseEntity.getBody();

        if (!(questionWithAnswersResponse instanceof Question)) {
            LOGGER.error("Exception when find question with answers", questionWithAnswersResponse);
            return responseEntity;
        }

        final Question question = (Question) questionWithAnswersResponse;
        final List<Answer> answers = question.getAnswers();

        if (answers.isEmpty()) {
            return ok(new StatisticsResponse(0, 0));
        }

        int totalNumberOfResponses = 0;
        for (Answer answer : answers) {
            if (answer.isSelected()) {
                totalNumberOfResponses++;
            }
        }

        return ok(new StatisticsResponse(totalNumberOfResponses, totalNumberOfResponses * 100 / answers.size()));
    }
}
