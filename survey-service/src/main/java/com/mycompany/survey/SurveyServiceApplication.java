package com.mycompany.survey;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SurveyServiceApplication {

    public static void main(final String[] args) {
        SpringApplication.run(SurveyServiceApplication.class, args);
    }
}
