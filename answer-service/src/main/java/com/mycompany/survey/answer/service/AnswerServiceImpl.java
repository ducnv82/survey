package com.mycompany.survey.answer.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.mycompany.survey.answer.model.Answer;
import com.mycompany.survey.answer.model.CreateAnswerRequest;
import com.mycompany.survey.answer.model.SelectAnswerRequest;
import com.mycompany.survey.answer.model.UpdateAnswerRequest;
import com.mycompany.survey.answer.model.UpdateSurveyRequest;
import com.mycompany.survey.answer.repository.AnswerRepository;
import com.mycompany.survey.core.entity.Question;
import com.mycompany.survey.model.ErrorResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import static java.util.stream.Collectors.toList;
import static org.springframework.beans.BeanUtils.copyProperties;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.ResponseEntity.ok;

@Service
@Validated
public class AnswerServiceImpl implements AnswerService {

    @Autowired
    private AnswerRepository answerRepository;

    @Override
    @Transactional
    public Answer createAnswer(@Valid final CreateAnswerRequest request) {
        final com.mycompany.survey.core.entity.Answer answer = new com.mycompany.survey.core.entity.Answer(
                                       new Question(request.getQuestionId()), request.getContent());
        answerRepository.save(answer);
        return ofEntity(answer);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Answer> findAllAnswersByQuestionId(@Valid @NotNull final UUID questionId) {
        return answerRepository.findByQuestionId(questionId).stream().map(entity -> ofEntity(entity)).collect(toList());
    }

    @Override
    @Transactional
    public ResponseEntity<Object> updateAnswer(@Valid final UpdateAnswerRequest request) {
        final UUID questionId = request.getId();
        final Optional<com.mycompany.survey.core.entity.Answer> answerData = answerRepository.findById(questionId);
        if (answerData.isPresent()) {
            final com.mycompany.survey.core.entity.Answer entity = answerData.get();
            entity.setContent(request.getContent());
            entity.setQuestion(new Question(request.getQuestionId()));
            entity.setSelected(request.isSelected());
            answerRepository.saveAndFlush(entity);
            return ok(ofEntity(entity));
        }
        return new ResponseEntity<>(new ErrorResponse("Cannot find answer with id " + questionId, NOT_FOUND.value()), NOT_FOUND);
    }

    @Override
    public ResponseEntity<Object> deleteAnswer(@Valid @NotNull final UUID id) {
        final Optional<com.mycompany.survey.core.entity.Answer> answerData = answerRepository.findById(id);
        if (answerData.isPresent()) {
            answerRepository.delete(answerData.get());
            return ok().build();
        }
        return new ResponseEntity<>(new ErrorResponse("Cannot find answer with id " + id, NOT_FOUND.value()), NOT_FOUND);
    }

    @Override
    @Transactional
    public void updateSelectedAnswers(@Valid final UpdateSurveyRequest request) {
        List<com.mycompany.survey.core.entity.Answer> answers = new ArrayList<>();
        answerRepository.findByQuestionId(request.getQuestionId()).forEach(entity -> {
            final SelectAnswerRequest selectAnswerRequest = findAnswer(entity.getId(), request.getSelectAnswerRequests());
            if (selectAnswerRequest != null) {
                entity.setSelected(selectAnswerRequest.isSelected());
                answers.add(entity);
            }
        });

        if (!answers.isEmpty()) {
            answerRepository.saveAll(answers);
        }
    }

    private SelectAnswerRequest findAnswer(final UUID answerId, final List<SelectAnswerRequest> selectAnswerRequests) {
        for (SelectAnswerRequest selectAnswerRequest : selectAnswerRequests) {
            if (selectAnswerRequest.getId().equals(answerId)) {
                return selectAnswerRequest;
            }
        }
        return null;
    }

    private Answer ofEntity(final com.mycompany.survey.core.entity.Answer entity) {
        final Answer answer = new Answer();
        copyProperties(entity, answer);
        return answer;
    }
}
