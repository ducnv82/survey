package com.mycompany.survey.answer.repository;

import java.util.List;
import java.util.UUID;

import com.mycompany.survey.core.entity.Answer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AnswerRepository extends JpaRepository<Answer, UUID> {

    List<Answer> findByQuestionId(UUID questionId);
}
