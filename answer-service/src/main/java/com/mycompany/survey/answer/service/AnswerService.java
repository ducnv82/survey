package com.mycompany.survey.answer.service;

import java.util.List;
import java.util.UUID;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.mycompany.survey.answer.model.Answer;
import com.mycompany.survey.answer.model.CreateAnswerRequest;
import com.mycompany.survey.answer.model.UpdateAnswerRequest;
import com.mycompany.survey.answer.model.UpdateSurveyRequest;
import org.springframework.http.ResponseEntity;

public interface AnswerService {

    Answer createAnswer(@Valid CreateAnswerRequest request);

    List<Answer> findAllAnswersByQuestionId(@Valid @NotNull UUID questionId);

    ResponseEntity<Object> updateAnswer(@Valid UpdateAnswerRequest request);

    ResponseEntity<Object> deleteAnswer(@Valid @NotNull UUID id);

    void updateSelectedAnswers(@Valid UpdateSurveyRequest request);
}
