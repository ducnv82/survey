package com.mycompany.survey.answer.controller;

import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import com.mycompany.survey.answer.model.Answer;
import com.mycompany.survey.answer.model.CreateAnswerRequest;
import com.mycompany.survey.answer.model.UpdateAnswerRequest;
import com.mycompany.survey.answer.model.UpdateSurveyRequest;
import com.mycompany.survey.answer.service.AnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpStatus.CREATED;

@RestController
@RequestMapping("/api/answers")
public class AnswerController {

    @Autowired
    private AnswerService answerService;

    @PostMapping
    @ResponseStatus(CREATED)
    public Answer createAnswer(@Valid @RequestBody final CreateAnswerRequest request) {
        return answerService.createAnswer(request);
    }

    @GetMapping
    public List<Answer> findAllAnswersByQuestionId(@RequestParam final UUID questionId) {
        return answerService.findAllAnswersByQuestionId(questionId);
    }

    @PutMapping(path = "{id}")
    public ResponseEntity<Object> updateAnswer(@PathVariable final UUID id, @Valid @RequestBody final UpdateAnswerRequest request) {
        request.setId(id);
        return answerService.updateAnswer(request);
    }

    @PutMapping(path = "/batch-update-selection")
    public void updateSelectedAnswers(@Valid @RequestBody final UpdateSurveyRequest request) {
        answerService.updateSelectedAnswers(request);
    }

    @DeleteMapping(path = "{id}")
    public ResponseEntity<Object> deleteAnswer(@PathVariable final UUID id) {
        return answerService.deleteAnswer(id);
    }
}
