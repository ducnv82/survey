package com.mycompany.survey.question.model;

import java.util.UUID;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UpdateQuestionRequest {

    @NotNull
    private UUID id;

    @NotBlank
    @Size(max = 255)
    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(final String content) {
        this.content = content;
    }

    public UUID getId() {
        return id;
    }

    public void setId(final UUID id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "UpdateQuestionRequest [id=" + id + ", content=" + content + "]";
    }
}
