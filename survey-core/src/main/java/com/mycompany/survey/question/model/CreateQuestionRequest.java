package com.mycompany.survey.question.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class CreateQuestionRequest {

    @NotBlank
    @Size(max = 255)
    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(final String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "CreateQuestionRequest [content=" + content + "]";
    }
}
