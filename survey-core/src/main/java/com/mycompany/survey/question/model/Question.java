package com.mycompany.survey.question.model;

import java.util.List;

import com.mycompany.survey.answer.model.Answer;
import com.mycompany.survey.model.BaseModel;

import static java.util.Collections.emptyList;

public class Question extends BaseModel {

    private String content;
    private List<Answer> answers = emptyList();

    public String getContent() {
        return content;
    }

    public void setContent(final String content) {
        this.content = content;
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(final List<Answer> answers) {
        this.answers = answers;
    }
}
