package com.mycompany.survey.model;

public class StatisticsResponse {

    private final int totalNumberOfResponses;
    private final int possibleAnswersInPercent;

    public StatisticsResponse(final int totalNumberOfResponses, final int possibleAnswersInPercent) {
        this.totalNumberOfResponses = totalNumberOfResponses;
        this.possibleAnswersInPercent = possibleAnswersInPercent;
    }

    public int getTotalNumberOfResponses() {
        return totalNumberOfResponses;
    }

    public int getPossibleAnswersInPercent() {
        return possibleAnswersInPercent;
    }
}
