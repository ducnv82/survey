package com.mycompany.survey.model;

public class ErrorResponse {

    private final String errorMessage;
    private final int statusCode;

    public ErrorResponse(final String errorMessage, final int statusCode) {
        this.errorMessage = errorMessage;
        this.statusCode = statusCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public int getStatusCode() {
        return statusCode;
    }

    @Override
    public String toString() {
        return "ErrorResponse [errorMessage=" + errorMessage + ", statusCode=" + statusCode + "]";
    }
}
