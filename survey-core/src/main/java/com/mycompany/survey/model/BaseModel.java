package com.mycompany.survey.model;

import java.util.UUID;

public abstract class BaseModel {

    protected UUID id;
    protected long version;

    public UUID getId() {
        return id;
    }

    public void setId(final UUID id) {
        this.id = id;
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(final long version) {
        this.version = version;
    }
}
