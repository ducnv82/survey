package com.mycompany.survey.answer.model;

import java.util.UUID;

import com.mycompany.survey.model.BaseModel;

public class Answer extends BaseModel {

    private boolean selected;

    private UUID questionId;

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(final boolean selected) {
        this.selected = selected;
    }

    public UUID getQuestionId() {
        return questionId;
    }

    public void setQuestionId(final UUID questionId) {
        this.questionId = questionId;
    }
}
