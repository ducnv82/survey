package com.mycompany.survey.answer.model;

import java.util.UUID;

import javax.validation.constraints.NotNull;

public class SelectAnswerRequest {

    @NotNull
    private UUID id;

    private boolean selected;

    public UUID getId() {
        return id;
    }

    public void setId(final UUID id) {
        this.id = id;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(final boolean selected) {
        this.selected = selected;
    }
}
