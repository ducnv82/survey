package com.mycompany.survey.answer.model;

import java.util.UUID;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class UpdateAnswerRequest {

    @NotNull
    private UUID id;

    @NotNull
    private UUID questionId;

    @NotBlank
    private String content;

    private boolean selected;

    public UUID getQuestionId() {
        return questionId;
    }

    public void setQuestionId(final UUID questionId) {
        this.questionId = questionId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(final String content) {
        this.content = content;
    }

    public UUID getId() {
        return id;
    }

    public void setId(final UUID id) {
        this.id = id;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(final boolean selected) {
        this.selected = selected;
    }
}
