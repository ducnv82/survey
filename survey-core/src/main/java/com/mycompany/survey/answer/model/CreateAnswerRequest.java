package com.mycompany.survey.answer.model;

import java.util.UUID;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class CreateAnswerRequest {

    @NotNull
    private UUID questionId;

    @NotBlank
    private String content;

    public UUID getQuestionId() {
        return questionId;
    }

    public void setQuestionId(final UUID questionId) {
        this.questionId = questionId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(final String content) {
        this.content = content;
    }
}
