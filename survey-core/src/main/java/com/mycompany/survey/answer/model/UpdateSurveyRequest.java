package com.mycompany.survey.answer.model;

import java.util.List;
import java.util.UUID;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class UpdateSurveyRequest {

    @NotNull
    private UUID questionId;

    @NotEmpty
    @Valid
    private List<SelectAnswerRequest> selectAnswerRequests;

    public UUID getQuestionId() {
        return questionId;
    }

    public void setQuestionId(final UUID questionId) {
        this.questionId = questionId;
    }

    public List<SelectAnswerRequest> getSelectAnswerRequests() {
        return selectAnswerRequests;
    }

    public void setSelectAnswerRequests(final List<SelectAnswerRequest> selectAnswerRequests) {
        this.selectAnswerRequests = selectAnswerRequests;
    }
}
