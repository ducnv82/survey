package com.mycompany.survey.core.entity;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import static javax.persistence.FetchType.LAZY;

@Entity
@Table(name = "answer", indexes = @Index(columnList = "question_id", name = "answer_question_id_idx"))
public class Answer extends BaseEntity {

    @Column
    private boolean selected;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "question_id", nullable = false, foreignKey = @ForeignKey(name = "answer_question_id_fkey"))
    private Question question;

    @Column(name = "question_id", insertable = false, updatable = false)
    private UUID questionId;

    public Answer() {}

    public Answer(final Question question, final String content) {
        this.question = question;
        this.content = content;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(final boolean selected) {
        this.selected = selected;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(final Question question) {
        this.question = question;
    }

    public UUID getQuestionId() {
        return questionId;
    }
}
