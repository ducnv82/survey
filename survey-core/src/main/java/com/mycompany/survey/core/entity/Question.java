package com.mycompany.survey.core.entity;

import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "question")
public class Question extends BaseEntity {

    public Question() {}

    public Question(final String content) {
        this.content = content;
    }

    public Question(final UUID id) {
        this.id = id;
    }
}
