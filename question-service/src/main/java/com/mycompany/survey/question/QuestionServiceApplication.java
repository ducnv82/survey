package com.mycompany.survey.question;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuestionServiceApplication {

    public static void main(final String[] args) {
        SpringApplication.run(QuestionServiceApplication.class, args);
    }
}
