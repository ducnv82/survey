package com.mycompany.survey.question.service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.mycompany.survey.model.ErrorResponse;
import com.mycompany.survey.question.model.CreateQuestionRequest;
import com.mycompany.survey.question.model.Question;
import com.mycompany.survey.question.model.UpdateQuestionRequest;
import com.mycompany.survey.question.repository.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import static java.util.stream.Collectors.toList;
import static org.springframework.beans.BeanUtils.copyProperties;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.ResponseEntity.ok;

@Service
@Validated
public class QuestionServiceImpl implements QuestionService {

    @Autowired
    private QuestionRepository questionRepository;

    @Override
    @Transactional
    public Question createQuestion(@Valid final CreateQuestionRequest request) {
        final com.mycompany.survey.core.entity.Question question = new com.mycompany.survey.core.entity.Question(request.getContent());
        return ofEntity(questionRepository.save(question));
    }

    @Override
    @Transactional(readOnly = true)
    public List<Question> findAllQuestions() {
        return questionRepository.findAll().stream().map(entity -> ofEntity(entity)).collect(toList());
    }

    @Override
    @Transactional
    public ResponseEntity<Object> updateQuestion(@Valid final UpdateQuestionRequest request) {
        final UUID questionId = request.getId();
        final Optional<com.mycompany.survey.core.entity.Question> questionData = questionRepository.findById(questionId);
        if (questionData.isPresent()) {
            final com.mycompany.survey.core.entity.Question entity = questionData.get();
            entity.setContent(request.getContent());
            questionRepository.saveAndFlush(entity);
            return ok(ofEntity(entity));
        }
        return new ResponseEntity<>(new ErrorResponse("Cannot find question with id " + questionId, NOT_FOUND.value()), NOT_FOUND);
    }

    @Override
    @Transactional
    public ResponseEntity<Object> deleteQuestion(@Valid @NotNull final UUID id) {
        final Optional<com.mycompany.survey.core.entity.Question> questionData = questionRepository.findById(id);
        if (questionData.isPresent()) {
            questionRepository.delete(questionData.get());
            return ok().build();
        }
        return new ResponseEntity<>(new ErrorResponse("Cannot find question with id " + id, NOT_FOUND.value()), NOT_FOUND);
    }

    @Override
    @Transactional(readOnly = true)
    public ResponseEntity<Object> findOneQuestion(@Valid @NotNull final UUID id) {
        final Optional<com.mycompany.survey.core.entity.Question> questionData = questionRepository.findById(id);
        if (questionData.isPresent()) {
            return ok(ofEntity(questionData.get()));
        }
        return new ResponseEntity<>(new ErrorResponse("Cannot find question with id " + id, NOT_FOUND.value()), NOT_FOUND);
    }

    private Question ofEntity(final com.mycompany.survey.core.entity.Question entity) {
        final Question question = new Question();
        copyProperties(entity, question);
        return question;
    }
}
