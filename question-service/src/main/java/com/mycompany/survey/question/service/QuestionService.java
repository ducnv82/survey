package com.mycompany.survey.question.service;

import java.util.List;
import java.util.UUID;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.mycompany.survey.question.model.CreateQuestionRequest;
import com.mycompany.survey.question.model.Question;
import com.mycompany.survey.question.model.UpdateQuestionRequest;
import org.springframework.http.ResponseEntity;

public interface QuestionService {

    Question createQuestion(@Valid CreateQuestionRequest request);

    List<Question> findAllQuestions();

    ResponseEntity<Object> updateQuestion(@Valid UpdateQuestionRequest request);

    ResponseEntity<Object> deleteQuestion(@Valid @NotNull UUID id);

    ResponseEntity<Object> findOneQuestion(@Valid @NotNull UUID id);
}
