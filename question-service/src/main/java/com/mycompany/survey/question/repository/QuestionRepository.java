package com.mycompany.survey.question.repository;

import java.util.UUID;

import com.mycompany.survey.core.entity.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestionRepository extends JpaRepository<Question, UUID> {
    //EMPTY
}
