package com.mycompany.survey.question.controller;

import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import com.mycompany.survey.question.model.CreateQuestionRequest;
import com.mycompany.survey.question.model.Question;
import com.mycompany.survey.question.model.UpdateQuestionRequest;
import com.mycompany.survey.question.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpStatus.CREATED;

@RestController
@RequestMapping("/api/questions/")
public class QuestionController {

    @Autowired
    private QuestionService questionService;

    @PostMapping
    @ResponseStatus(CREATED)
    public Question createQuestion(@Valid @RequestBody final CreateQuestionRequest request) {
        return questionService.createQuestion(request);
    }

    @GetMapping
    public List<Question> findAllQuestions() {
        return questionService.findAllQuestions();
    }

    @GetMapping(path = "{id}")
    public ResponseEntity<Object> findOneQuestion(@PathVariable final UUID id) {
        return questionService.findOneQuestion(id);
    }

    @PutMapping(path = "{id}")
    public ResponseEntity<Object> updateQuestion(@PathVariable final UUID id, @Valid @RequestBody final UpdateQuestionRequest request) {
        request.setId(id);
        return questionService.updateQuestion(request);
    }

    @DeleteMapping(path = "{id}")
    public ResponseEntity<Object> deleteQuestion(@PathVariable final UUID id) {
        return questionService.deleteQuestion(id);
    }
}
