# The back-end of a survey application uses MicroServices with Spring Boot
The Java backend application should implement the following features:

*   add/edit/delete questions, answers
*   read a list of all questions    
*   read a question with all answers
*   respond to a survey
*   read the result for a particular question showing the total number of responses and distribution among the possible answers in percent.


# Microservices architecture:

1.  question-service is a spring boot web app running at the port 8084 to serve all functionality related to question. REST API(s) are in QuestionController. When this app runs in the first time, it will create tables question and answer (if not exists) in database.
2.  answer-service is a spring boot web app running at the port 8085 to serve all functionality related to answer. REST API(s) are in AnswerController. When this app runs in the first time, it will create tables question and answer (if not exists) in database.
3.  survey-service is a spring boot web app running at the port 8086 to serve all functionality related to survey. REST API(s) are in SurveyController. It uses REST requests to communicate with question-service and answer-service


*   survey-core is a reusable module used by all microservices
*   survey-parent is the parent project contains only a pom.xml file to manage dependencies, configurations for all modules  

# Prerequisite

1. Java: Install latest Oracle JDK 8, download at http://www.oracle.com/technetwork/java/javase/downloads/index.html
2. Install latest apache maven or at least version 3.5.4
3. Install PostgreSQL 9.6 

# Build and run the application

1.  Build: In the folder `survey-parent` run the command `mvn clean install` to build the project and install dependencies to local maven repository
2.  Run the app `question-service`: in the folder _question-service_  run the command `mvn spring-boot:run -DDATASOURCE_URL=[YOUR JDBC URL TO POSTGRESQL] -DDATASOURCE_USERNAME=[YOUR DATABASE USERNAME] -DDATASOURCE_PASSWORD=[YOUR DATABASE PASSWORD]`. Please remember to use actual values instead of values in the brackets. Then the app will be available at http://localhost:8084/api/questions
3.  Run the app `answer-service`: in the folder _answer-service_  run the command `mvn spring-boot:run -DDATASOURCE_URL=[YOUR JDBC URL TO POSTGRESQL] -DDATASOURCE_USERNAME=[YOUR DATABASE USERNAME] -DDATASOURCE_PASSWORD=[YOUR DATABASE PASSWORD]`. Please remember to use actual values instead of values in the brackets. Then the app will be available at http://localhost:8085/api/answers
4.  Run the app `survey-service`: in the folder _survey-service_  run the command `mvn spring-boot:run`. Then the app will be available at http://localhost:8086/api/surveys
  
Note: If you do not provide database information in system properties (or environment variables), it uses the default params for db connection with url **jdbc:postgresql://localhost:5432/postgres**, username: **postgres**, password: **postgres**

Or you can import the projects into an IDE like (Eclipse with spring tool suite) and run 3 projects in classes _QuestionServiceApplication_, _AnswerServiceApplication_, _SurveyServiceApplication_ as `Spring Boot App`  
You can edit JDBC params in application.properties.  

# Test the application
We can use [Postman](https://www.getpostman.com/ "Postman") to test REST APIs, import the Postman collection [survey.postman_collection.json](survey.postman_collection.json). Sample API steps in Postman collection:

1.  createQuestion
2.  createAnswer 1 in the question 1
3.  createAnswer 2 in the question 1
4.  findAllQuestions
5.  findQuestionWithAnswers
6.  doOneSurvey
7.  findStatisticsInOneQuestion


# Thought about requirements
Requirement: read the result for a particular question showing the total number of responses and distribution among the possible answers in percent.  

I implement: total number of responses is the checked answers, distribution among the possible answers in percent is the percent of checked answer(s) on the total of answers.  
Feel free to discuss the changes (if needed) for requirements.
